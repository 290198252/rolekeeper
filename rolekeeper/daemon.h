/*
 * @Author: your name
 * @Date: 2021-07-23 23:40:36
 * @LastEditTime: 2021-12-06 10:53:37
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edi
 * @FilePath: \generallib\general\src\function\daemon.h
 */

#pragma once

#if __cplusplus >= 201103L
#pragma  message("编译器支持c++11")
#endif 

#ifdef linux
#include <unistd.h>
#endif

#include <list>
#include<iostream>
#include <algorithm>
using namespace std;

#ifdef WIN32
#include <windows.h>
#include <Tlhelp32.h>
#include <psapi.h>
#endif
#include <string>
#include <vector>
#include <map>
#include <stdint.h>
#include <thread>
#include <stdio.h>
#include <errno.h>
#include <memory.h>
#include <functional>
#include <thread>


class DaemonizeMonitor{
public:
    typedef std::function<void(std::string msg)> CmdCallback;
    typedef std::function<void(void)> RestartCallback;

    DaemonizeMonitor(string path);
    int AddNewProcess(string);
    int StopMonitor(string);
    int StopProcess(string);
    int StartMonitor();
    int StartProcessCommand(std::string path);
    int SetRestartCallback(RestartCallback );
    int SetCommandCallback(CmdCallback);
private:
    std::vector<string> m_path_process;
    std::map<string,uint32_t> m_running_pid; 
    std::function<void(void)>  mCallback;
    CmdCallback mCmdCallback;
    std::thread *mCmdThread;

};
