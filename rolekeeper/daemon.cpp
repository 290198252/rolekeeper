#include "daemon.h"

#include <QDebug>

#pragma comment (lib,"Advapi32.lib")


typedef struct
{
    uint16_t pid;
    string process_name;
} RuningProcess;

vector<RuningProcess> RangeProcess();

#define BUF_SIZE 1024

std::wstring String2WString(const std::string& str_in)
{
    if (str_in.empty())
    {
        std::cout << "str_in is empty" << std::endl;
        return L"";
    }

    // 获取待转换的数据的长度
    int len_in = MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)str_in.c_str(), -1, NULL, 0);
    if (len_in <= 0)
    {
        std::cout << "The result of WideCharToMultiByte is Invalid!" << std::endl;
        return L"";
    }

    // 为输出数据申请空间
    std::wstring wstr_out;
    wstr_out.resize(len_in - 1, L'\0');

    // 数据格式转换
    int to_result = MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)str_in.c_str(), -1, (LPWSTR)wstr_out.c_str(), len_in);

    // 判断转换结果
    if (0 == to_result)
    {
        std::cout << "Can't transfer String to WString" << std::endl;
    }

    return wstr_out;
}

std::vector<std::string> splitString(std::string srcStr, std::string delimStr,bool repeatedCharIgnored)
{
    std::vector<std::string> resultStringVector;
    std::replace_if(srcStr.begin(), srcStr.end(), [&](const char& c){if(delimStr.find(c)!=std::string::npos){return true;}else{return false;}}/*pred*/, delimStr.at(0));//将出现的所有分隔符都替换成为一个相同的字符（分隔符字符串的第一个）
    size_t pos=srcStr.find(delimStr.at(0));
    std::string addedString="";
    while (pos!=std::string::npos) {
        addedString=srcStr.substr(0,pos);
        if (!addedString.empty()||!repeatedCharIgnored) {
            resultStringVector.push_back(addedString);
        }
        srcStr.erase(srcStr.begin(), srcStr.begin()+pos+1);
        pos=srcStr.find(delimStr.at(0));
    }
    addedString=srcStr;
    if (!addedString.empty()||!repeatedCharIgnored) {
        resultStringVector.push_back(addedString);
    }
    return resultStringVector;
}


int  DaemonizeMonitor::StartProcessCommand(std::string path)
{
    auto pos = path.find(".exe");
    if(pos != std::string::npos)
    {
        std::cout << "find it" << std::endl;
    }else{
        return -1;
    }


    auto tmp = splitString(path,"\\",true);
    std::string path1;
    for (auto itr = tmp.begin();itr != tmp.end();itr++){
        if(itr->find(".exe") == std::string::npos ){
            path1 += *itr +"/";
        }
    }

    HANDLE PipeReadHandle;
    HANDLE PipeWriteHandle;
    PROCESS_INFORMATION	ProcessInfo;
    SECURITY_ATTRIBUTES SecurityAttributes;
    STARTUPINFOA StartupInfo;
    BOOL  Success;
    //--------------------------------------------------------------------------
    //	Zero the structures.
    //--------------------------------------------------------------------------
    ZeroMemory( &StartupInfo,			sizeof( StartupInfo ));
    ZeroMemory( &ProcessInfo,			sizeof( ProcessInfo ));
    ZeroMemory( &SecurityAttributes,	sizeof( SecurityAttributes ));

    //--------------------------------------------------------------------------
    //	Create a pipe for the child's STDOUT.
    //--------------------------------------------------------------------------
    SecurityAttributes.nLength              = sizeof(SECURITY_ATTRIBUTES);
    SecurityAttributes.bInheritHandle       = TRUE;
    SecurityAttributes.lpSecurityDescriptor = NULL;

    Success = CreatePipe
    (
            &PipeReadHandle,		// address of variable for read handle
            &PipeWriteHandle,		// address of variable for write handle
            &SecurityAttributes,	// pointer to security attributes
            0						// number of bytes reserved for pipe (use default size)
    );

    if ( !Success )
    {
        int err = GetLastError();
        qDebug()<<"Error CreatePipe process"<<err;
        //ShowLastError(_T("Error creating pipe"));
        return -1;
    }

    //--------------------------------------------------------------------------
    //	Set up members of STARTUPINFO structure.
    //--------------------------------------------------------------------------
    StartupInfo.cb           = sizeof(STARTUPINFO);
    StartupInfo.dwFlags      = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
    StartupInfo.wShowWindow  = SW_HIDE;
    StartupInfo.hStdOutput   = PipeWriteHandle;
    StartupInfo.hStdError    = PipeWriteHandle;

    //----------------------------------------------------------------------------
    //	Create the child process.
    //----------------------------------------------------------------------------
    wchar_t dest[500];
    mbstowcs(dest,path.c_str(),path.size());
    wchar_t dest2[500];
    mbstowcs(dest2,path1.c_str(),path1.size());

    qDebug()<<QString::fromWCharArray(dest,path.size())<<QString::fromWCharArray(dest2,path1.size());
      Success = CreateProcessA
      (
          NULL,					// pointer to name of executable module
          (LPSTR)path.c_str(),	// command line
          NULL,					// pointer to process security attributes
          NULL,					// pointer to thread security attributes (use primary thread security attributes)
          TRUE,					// inherit handles
          0,						// creation flags
          NULL,					// pointer to new environment block (use parent's)
          path1.c_str(),	// pointer to current directory name
          &StartupInfo,			// pointer to STARTUPINFO
          &ProcessInfo			// pointer to PROCESS_INFORMATION
      );

      if ( !Success )
      {
              //ShowLastError(_T("Error creating process"));、
              int err = GetLastError();
              qDebug()<<"Error creating process"<<err<<QString::fromWCharArray(dest,path.size())
                     <<QString::fromWCharArray(dest2,path1.size());
              return -1;
      }



      char	PipeData[BUF_SIZE] = {0};
      DWORD	BytesLeftThisMessage = 0;
      DWORD	NumBytesRead;
      DWORD	TotalBytesAvailable = 0;
      for (;;)
      {
              NumBytesRead = 0;
              Success = PeekNamedPipe
              (
                      PipeReadHandle,				// handle to pipe to copy from
                      PipeData,					// pointer to data buffer
                      1,							// size, in bytes, of data buffer
                      &NumBytesRead,				// pointer to number of bytes read
                      &TotalBytesAvailable,		// pointer to total number of bytes available
                      &BytesLeftThisMessage		// pointer to unread bytes in this message
              );
              if ( !Success )
              {
                      break;
              }
              if (NumBytesRead)
              {
                      Success = ReadFile
                      (
                              PipeReadHandle,		// handle to pipe to copy from
                              PipeData,			// address of buffer that receives data
                              BUF_SIZE - 1,		// number of bytes to read
                              &NumBytesRead,		// address of number of bytes read
                              NULL				// address of structure for data for overlapped I/O
                      );
                      if ( !Success )
                      {
                              break;
                      }
                      //------------------------------------------------------------------
                      //	Zero-terminate the data.
                      //------------------------------------------------------------------
                      PipeData[NumBytesRead] = '\0';

                      //------------------------------------------------------------------
                      //	Replace backspaces with spaces.
                      //------------------------------------------------------------------
                      for ( DWORD ii = 0; ii < NumBytesRead; ii++ )
                      {
                              if ( PipeData[ii] == (L'\b') )
                              {
                                      PipeData[ii] = ' ';
                              }
                      }
                char data[3048];
                qDebug()<<PipeData;
                std::cout << (data);
                if(mCmdCallback.operator bool())
                    mCmdCallback(PipeData);
                }
              else
              {
                      //------------------------------------------------------------------
                      //	If the child process has completed, break out.
                      //------------------------------------------------------------------
                      if ( WaitForSingleObject(ProcessInfo.hProcess, 0) == WAIT_OBJECT_0 )	//lint !e1924 (warning about C-style cast)
                      {
                              break;
                      }
              }

      }

    //--------------------------------------------------------------------------
    //	Close handles.
    //--------------------------------------------------------------------------
    Success = CloseHandle(ProcessInfo.hThread);
    if ( !Success )
    {
    }

    Success = CloseHandle(ProcessInfo.hProcess);
    if ( !Success )
    {
    }

    Success = CloseHandle(PipeReadHandle);
    if ( !Success )
    {
    }

    Success = CloseHandle(PipeWriteHandle);
    if ( !Success )
    {
    }
}

vector<string> StripList(const char *in, const char *d)
{
    vector<string> ret;
    map<uint16_t,bool> pos;
    vector<uint16_t> pos_strip_continue;
    int lastpos = -1;

    if ((nullptr == in) || (nullptr == d))
    {
        return ret;
    }
    int len = strlen(in);
    int sublen = strlen(d);
    if (len <= 0)
        return ret;
    if (sublen <= 0)
        return ret;

    for (int i = 0; i < (len - sublen + 1); i++)
    {
        bool found = true;
        for (int j = 0; j < strlen(d); j++)
        {
            if (in[i + j] != d[j])
            {
                found = false;
            }
        }
        if (!found)
        {
            pos.insert(make_pair(i,true));
            i += sublen - 1;
            continue;
        }
    }
    if (pos.size() == 0)
    {
        ret.push_back(string(in));
        return ret;
    }
    bool started = true;
    uint16_t startpos = 0;  
    uint16_t endpos = 0;

    for(int i = 0;i < len - 1;i++){

        if(pos[i] == true){
            if(started){
                startpos = i;
                started = false;
            }
            if((pos[i + sublen] != true)){
                endpos = i;
                // std::cout<<startpos<<" "<<endpos<<"\r\n";
                ret.push_back(string(&in[startpos],&in[endpos + 1]));
                started = true;
            }
        }
        if(i == (len - 2)){
            i = len - 1;
            if(pos[i] == true){
                if(started){
                    startpos = i;
                    endpos = i;
                    // std::cout<<startpos<<" "<<endpos<<"\r\n";
                    ret.push_back(string(&in[startpos],&in[endpos + 1]));
                }else{
                    endpos = i;
                    // std::cout<<startpos<<" "<<endpos<<"\r\n";
                    ret.push_back(string(&in[startpos],&in[endpos + 1]));
                }
            }
  
        }
    }

    return ret;
}


#ifdef linux

inline bool existed(const std::string &name)
{
    return (access(name.c_str(), F_OK) != -1);
}

void _process_start(string path)
{

    auto file = popen(path.c_str(), "r");

    if (file < 0)
    {

        fprintf(stderr, "execl failed:%s", strerror(errno));

        return;
    }

    pclose(file);
}

vector<RuningProcess> RangeProcess()
{

    FILE *pstr;

    char cmd[128], buff[512], *p;

    vector<RuningProcess> ret;

    int iPID;

    int pidPosition = 1;

    int pInfoPosition = 7;

    memset(cmd, 0, sizeof(cmd));

    sprintf(cmd, "ps ");

    pstr = popen(cmd, "r");

    if (pstr == NULL)
    {

        return ret;
    }

    memset(buff, 0, sizeof(buff));

    bool first = true;

    while (1)
    {

        RuningProcess ins;

        fgets(buff, 512, pstr);

        if (first)
        {

            first = false;

            continue;
        }

        if (feof(pstr))

        {

            break;
        }

        auto trip = StripList(buff, " ");

        ins.pid = atoi(trip[0].c_str());

        ins.process_name = trip[3];

        ret.push_back(ins);
    }

    pclose(pstr);

    return ret;
}

#endif

#ifdef _WIN32

static BOOL KillProcess(DWORD dwPid)

{
    HANDLE hPrc;
    if (0 == dwPid)
        return FALSE;
    hPrc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwPid); // Opens handle to the process.
    if (!TerminateProcess(hPrc, 0)) // Terminates a process.
    {

        CloseHandle(hPrc);

        return FALSE;
    }
    else
        WaitForSingleObject(hPrc, 2000); // At most ,waite 2000  millisecond.
    CloseHandle(hPrc);
    return TRUE;
}

static BOOL KillProcessByName(const TCHAR *lpszProcessName)
{
    unsigned int pid = -1;
    BOOL retval = TRUE;
    if (lpszProcessName == NULL)
        return -1;

    DWORD dwRet = 0;
    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    PROCESSENTRY32 processInfo;
    processInfo.dwSize = sizeof(PROCESSENTRY32);
    int flag = Process32First(hSnapshot, &processInfo);
    // Find the process with name as same as lpszProcessName

    while (flag != 0)
    {
        printf("kill process find %s\r\n", processInfo.szExeFile);
        char dest[140];
        if (lstrcmpW(processInfo.szExeFile, lpszProcessName) == 0)
        {
            // Terminate the process.
            pid = processInfo.th32ProcessID;
            HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, TRUE, pid);
            printf("kill process pid %d\r\n", pid);
            if (TerminateProcess(hProcess, 0) != TRUE)
            { // Failed to terminate it.
                retval = FALSE;
                break;
            }
        }
        flag = Process32Next(hSnapshot, &processInfo);
    } // while (flag != 0)
    CloseHandle(hSnapshot);
    if (pid == -1)
        return FALSE;
    return retval;
}

static BOOL SetProcessPrivilege(const char *lpName, BOOL opt)

{
    HANDLE tokenhandle;
    TOKEN_PRIVILEGES NewState;
    if (OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &tokenhandle))
    {
        wchar_t dest[150];
        mbstowcs(dest,lpName,strlen(lpName));
        LookupPrivilegeValue(NULL, dest, &NewState.Privileges[0].Luid);
        NewState.PrivilegeCount = 1;
        NewState.Privileges[0].Attributes = opt != 0 ? 2 : 0;
        AdjustTokenPrivileges(tokenhandle, FALSE, &NewState, sizeof(NewState), NULL, NULL);
        CloseHandle(tokenhandle);
        return 1;
    }
    else

    {
        return 0;
    }
}

static int test_fork()
{
    char szCommandLine[] = "D:\\game\\The Legend of Zelda Breath of the Wild\\cemu\\cemu.exe";
    STARTUPINFO si = {sizeof(si)};
    PROCESS_INFORMATION pi;
    si.dwFlags = STARTF_USESHOWWINDOW; //指定wShowWindow成员有效
    si.wShowWindow = TRUE; //此成员设为TRUE的话则显示新建进程的主窗
    wchar_t dest[350];
    mbstowcs(dest,szCommandLine,strlen(szCommandLine));
    BOOL bRet = CreateProcess(
        NULL, //不在此指定可执行文件的文件名
        dest, //命令行参
        NULL, //默认进程安全
        NULL, //默认进程安全
        FALSE, //指定当前进程内句柄不可以被子进程继承
        CREATE_NEW_CONSOLE, //为新进程创建一个新的控制台窗口
        NULL, //使用本进程的环境变量
        NULL, //使用本进程的驱动器和目录
        &si,
        &pi);
    if (bRet)
    {

        //不使用的句柄最好关
        CloseHandle(pi.hThread);
        CloseHandle(pi.hProcess);
        printf("new process id  %d\n", pi.dwProcessId);
        printf("new thread id %d\n", pi.dwThreadId);
    }
    return 0;
}

#endif

DaemonizeMonitor::DaemonizeMonitor(string path)
  :mCmdThread(nullptr)
{
#ifdef linux
    auto process = RangeProcess();
    for (auto itr = process.begin(); itr != process.end(); itr++)
    {
        std::cout << itr->process_name << std::endl;
        m_pids[itr->process_name.substr(0, itr->process_name.size() - 1)] = itr->pid;
    }
#endif
    auto strip = StripList(path.c_str(), "/");
    if(strip.size() > 0){
        std::cout<< strip[strip.size() - 1]<<std::endl;
        if(m_running_pid[strip[strip.size() - 1]] > 0){
        }else{
//            StartProcessCommand(path);
//         /   this->AddNewProcess(path);
        }
    }

}

#ifdef linux
void start_process(string path)
{

    std::thread p(_process_start, path);

    p.detach();
}
#endif

int DaemonizeMonitor::AddNewProcess(string path) {
    HANDLE hPipeOutputRead=NULL;
    HANDLE hPipeOutputWrite=NULL;
    HANDLE hPipeInputRead=NULL;
    HANDLE hPipeInputWrite=NULL;
    if (path == "") {
        return -1;
    }
    this->m_path_process.push_back(path);
    if(mCallback.operator bool())
        mCallback();

#ifdef linux
    if (!existed(path))
    {
        return -1;
    }
    start_process(path);
    usleep(100000);
    auto strip = StripList(path.c_str(), "/");
    
    auto process = RangeProcess();
    for (auto itr = process.begin(); itr != process.end(); itr++)
    {
        m_pids[itr->process_name.substr(0, itr->process_name.size() - 1)] = itr->pid;
    }
    if (m_pids[strip[strip.size() - 1]]  > 0)
        this->m_running_pid[strip[strip.size() - 1]] = m_pids[strip[strip.size() - 1]];
#endif
#ifdef _WIN32
    STARTUPINFO si = {sizeof(si)};
    PROCESS_INFORMATION pi;
    si.dwFlags =  STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
    si.wShowWindow = SW_HIDE; //此成员设为TRUE的话则显示新建进程的主窗

    wchar_t dest[150] = {0};
    mbstowcs(dest,(LPSTR)path.c_str(),strlen((LPSTR)path.c_str()));

    //创建管道
    SECURITY_ATTRIBUTES sa={0};
    sa.nLength = sizeof(sa);
    sa.bInheritHandle = TRUE;
    sa.lpSecurityDescriptor = NULL;

    //为标准输出重定向创建管道
    CreatePipe(&hPipeOutputRead,  // read handle
            &hPipeOutputWrite, // write handle
            &sa,      // security attributes
            0      // number of bytes reserved for pipe - 0 default
            );

    //为标准输入重定向创建管道
    CreatePipe(&hPipeInputRead,  // read handle
            &hPipeInputWrite, // write handle
            &sa,      // security attributes
            0      // number of bytes reserved for pipe - 0 default
            );

    //创建进程
    //使子进程使用hPipeOutputWrite作为标准输出使用hPipeInputRead作为标准输入，并使子进程在后台运行
    si.cb = sizeof(si);
    si.dwFlags     = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
    si.wShowWindow = SW_HIDE;
    si.hStdInput   = hPipeInputRead;
    si.hStdOutput  = hPipeOutputRead;
    si.hStdError   = hPipeOutputWrite;

    BOOL bRet = CreateProcess(
        NULL, //不在此指定可执行文件的文件名
        dest, //命令行参
        NULL, //默认进程安全
        NULL, //默认进程安全
        FALSE, //指定当前进程内句柄不可以被子进程继承
        0, //为新进程创建一个新的控制台窗口
        NULL, //使用本进程的环境变量
        L"G:\\project\\golang\\src\\background", //使用本进程的驱动器和目录
        &si,
        &pi);
    CHAR szBuffer[256];
    memset(szBuffer,0,sizeof(szBuffer));
    DWORD dwNumberOfBytesRead = 0;

    CloseHandle(hPipeOutputWrite);
    CloseHandle(hPipeInputRead);


    while(1)
    {
            bool bTest;
            bTest = ReadFile(
                    hPipeOutputRead,      // handle of the read end of our pipe
                    &szBuffer,            // address of buffer that receives data
                    sizeof(szBuffer),     // number of bytes to read
                    &dwNumberOfBytesRead, // address of number of bytes read
                    NULL                  // non-overlapped.
                    );

            if (bTest){
              qDebug()<<szBuffer;
            }
            else
            {
            }
    }

    if (bRet) {
        //不使用的句柄最好关
        CloseHandle(pi.hThread);
        CloseHandle(pi.hProcess);
        printf("new process id  %d\n", pi.dwProcessId);
        this->m_running_pid[path] = pi.dwProcessId;
        return 0;
    }
    return -1;
#endif
}

int DaemonizeMonitor::StartMonitor()
{
#ifdef _WIN32
    DWORD Proc_pid[10240], Retn_bytes, Proc_count, Retn_bytes2;
    unsigned int i;
    HMODULE hMod[1024];
    HANDLE hProcess;
    char szModName[MAX_PATH];
    while (true)
    {
        /* code */
        Sleep(5000);
        for (auto itr = m_running_pid.begin(); itr != m_running_pid.end(); itr++)
        {
            bool found = false;
            if (EnumProcesses(Proc_pid, sizeof(Proc_pid), &Retn_bytes))
            {
                Proc_count = Retn_bytes / sizeof(DWORD);
                SetProcessPrivilege("SeDebugPrivilege", 1);
                for (i = 0; i < Proc_count; i++)
                {
                    hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, Proc_pid[i]);
                    if (hProcess != NULL)
                    {
                        wchar_t dest[150] = {0};
                        wchar_t dest2[150] = {0};

                        mbstowcs(dest,szModName,strlen(szModName));
                        EnumProcessModules(hProcess, hMod, sizeof(hMod), &Retn_bytes2);
                        GetModuleFileNameEx(hProcess, hMod[0], dest, sizeof(szModName));
                        qDebug()<<QString::asprintf("PID=%d %d", Proc_pid[i],itr->second)
                               <<QString::fromWCharArray(dest,lstrlenW(dest));

                        mbstowcs(dest2,itr->first.c_str(),itr->first.size());

                        if (lstrcmpW(dest,dest2) == 0)
                        {
                            found = true;
                            break;
                        }
                    }
                    CloseHandle(hProcess);
                }
                SetProcessPrivilege("SeDebugPrivilege", 0);
                if (!found)
                {
                    // 没找到该应用实例就重启应
                    auto it = m_path_process.begin();
                    while (it != m_path_process.end())
                    {
                        if (*it == itr->first)
                        {
                            it = m_path_process.erase(it);
                        }
                        else
                            it++;
                    }
                    for (auto it = m_path_process.begin(); it != m_path_process.end(); it++)
                    {
                        std::cout << *it << std::endl;
                    }
//                    this->AddNewProcess(itr->first);
                    StartProcessCommand(itr->first);
                }
            }
        }
    }
    return 0;
#endif

#ifdef linux
    while (true){
        auto process = RangeProcess();
        usleep(50000);
        for (auto itr = m_running_pid.begin(); itr != m_running_pid.end(); itr++)
        {
            bool found = false;
            for (auto itr2 = process.begin(); itr2 != process.end(); itr2++)
            {
                if (itr->second == itr2->pid)
                {
                    // std::cout << itr->second << itr2->pid << std::endl;
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                // 没找到该应用实例就重启应
                auto it = m_path_process.begin();
                while (it != m_path_process.end())
                {
                    if (*it == itr->first)
                    {
                        it = m_path_process.erase(it);
                    }
                    else
                        it++;
                }
                for (auto it = m_path_process.begin(); it != m_path_process.end(); it++)
                {
                    std::cout << *it << std::endl;
                }
                std::cout << "add new process" << std::endl;
                if (0 > this->AddNewProcess(itr->first))
                    std::cout << "error not found" << std::endl;
            }            
        }
    }
#endif
}

int DaemonizeMonitor::SetRestartCallback(RestartCallback b)
{
    this->mCallback = b;
  return 0;
}

int DaemonizeMonitor::SetCommandCallback(CmdCallback callback)
{
  this->mCmdCallback = callback;
  return 0;
}

int DaemonizeMonitor::StopMonitor(string)
{
#ifdef _WIN32
    return 0;
#endif
}

int DaemonizeMonitor::StopProcess(string path)
{
    if (path == "")
    {
        return -1;
    }
#ifdef _WIN32
    if (m_running_pid.find(path) != m_running_pid.end())
    {
        DWORD pid = m_running_pid.at(path);
        m_running_pid.erase(path);
        if (KillProcess(pid))
            return 0;
        else
        {
            return -1;
        }
    }
    return 0;
#endif
#ifdef linux
#endif
}
