#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QSystemTrayIcon>
#include <QFileDialog>

#include <daemon.h>
#include <QTime>
#include <QTimer>
#include "global.h"

#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif



MainWindow::MainWindow(QWidget *parent)
    : QssMainWindow(parent)
    , ui(new Ui::MainWindow)
    , mStart(false)
    ,mGuard(nullptr)
{
    ui->setupUi(this);
    setWindowFlags( Qt::WindowStaysOnTopHint);
    auto p = GetConfig();
    qDebug()<<p->path;
    ui->lineEdit->setText(p->path);
    QSystemTrayIcon* m_tray; //托盘类

    QMenu* m_menu; //托盘菜单

    QAction* m_resetAction; //托盘按钮

    QAction* m_quitAction; //托盘按钮
    m_tray = new QSystemTrayIcon(this);//实例化
    QPixmap m_logo(":/dog.ico");

    m_tray->setIcon(QIcon(m_logo));//设置图标
    m_tray->show();
    connect(m_tray,&QSystemTrayIcon::activated,this,&MainWindow::TrayIconAction);
    m_menu = new QMenu(this);
    m_resetAction = new QAction(this);
    m_resetAction->setText("show");
    m_quitAction = new QAction(this);
    m_resetAction->setIcon(QIcon(m_logo));
    m_quitAction->setText("quit");
    m_quitAction->setIcon(QIcon(m_logo));
    connect(m_quitAction,&QAction::triggered,qApp,&QApplication::quit);
    connect(m_resetAction,&QAction::triggered,this,&MainWindow::restory);

    m_tray->setContextMenu(m_menu);//设置托盘菜单
    m_menu->addAction(m_resetAction);
    m_menu->addAction(m_quitAction);

    QTimer *p1 = new QTimer;
    this->ui->textEdit->setStyleSheet("background:black;color:white");
    connect(p1,&QTimer::timeout,[&](){
        mMux.lock();
        if(this->mLog.size() > 0)
          this->ui->textEdit->append(this->mLog);
        this->mLog = "";
        mMux.unlock();

    });
    p1->start(10);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::GuadDone()
{
    qDebug()<<"GuadDone";
}

void MainWindow::AddLog(QString b)
{
    mMux.lock();
    this->mLog += QDateTime::currentDateTime().toString("[yyyy-MM-dd hh:mm:ss]: ")
            +b + "\r\n";
    mMux.unlock();
}


void MainWindow::on_pushButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(
            this,
            tr("open a file."),
            "D:/",
            tr("exe(*.exe);;"));
        if (fileName != "") {
            m_path = fileName;
            ui->lineEdit->setText(m_path);
        }
}


void MainWindow::on_pushButton_2_clicked()
{
    if((ui->lineEdit->text() != "") && !mStart){
        if(mGuard == nullptr){
            mGuard = new ASyncGuard(this);
        }
        connect(mGuard,&ASyncGuard::Done,
                this,&MainWindow::GuadDone);
        mGuard->Start(new QString(ui->lineEdit->text()));
        mStart = true;
        ui->pushButton_2->setText("end");
        AddLog(QString("start guard ") +  ui->lineEdit->text() );
    }else if(mStart){
        mStart = true;
        ui->pushButton_2->setText("start ");
    }
}


void MainWindow::on_pushButton_3_clicked()
{
    AppConfig *p = new AppConfig;
    p->path = ui->lineEdit->text();
    SetConfig(p);
}

