#include "mainwindow.h"

#include <QApplication>

#include <Windows.h>
#include <stdio.h>
#include <tchar.h>
#include "global.h"

#define BUFFSZ 4096

HANDLE g_hChildStd_IN_Rd = NULL;
HANDLE g_hChildStd_IN_Wr = NULL;
HANDLE g_hChildStd_OUT_Rd = NULL;
HANDLE g_hChildStd_OUT_Wr = NULL;



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.setMinimumHeight(400);
    w.setMinimumWidth(600);
    w.SetTitleHeight(35);
    w.SetTrayIcon(new QIcon());
    w.setWindowIcon(QIcon("://dog.ico"));

    w.show();
    return a.exec();
}
