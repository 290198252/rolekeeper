#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "Qss.h"
#include <QMainWindow>
#include <QSystemTrayIcon>
#include "daemon.h"
#include <QDebug>
#include <QJsonObject>
#include <QMutex>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class ASyncGuard;

typedef struct {
    QString last_program_path; // 上次打开的路�
}Config;

class MainWindow : public QssMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void TrayIconAction(QSystemTrayIcon::ActivationReason reason)
    {

        if(reason == QSystemTrayIcon::Trigger)
            this->showNormal();
    }
    void restory(){
        this->showNormal();
    }
    void GuadDone();
    void AddLog(QString);
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::MainWindow *ui;
    QString m_path;
    ASyncGuard *mGuard;
    bool mStart;
    QString mLog;
    QMutex mMux;
};

class ASyncGuard :public QSSASyncProcess{
public:
    ASyncGuard(MainWindow *parent){
        mParent = parent;
    }
    void Run(void *ptr) {
        QString path = *((QString*)ptr);
        qDebug()<<path;
        path.replace("/","\\");
        mDaemon = new DaemonizeMonitor(path.toStdString().c_str());
        mDaemon->SetCommandCallback([&](std::string d){
          this->mParent->AddLog(QString::fromStdString(d));
        });
        mDaemon->SetRestartCallback([&](){
            mMux.lock();
            this->mParent->AddLog(QString("detect close,reopen ") + path);
            mMux.unlock();
        });
        mDaemon->StartMonitor();

    }
    DaemonizeMonitor *mDaemon;
    MainWindow *mParent;
    QMutex mMux;

};
#endif // MAINWINDOW_H
