cmake_minimum_required(VERSION 3.10)
project(superdog_server)
MESSAGE(STATUS "operation system is ${CMAKE_SYSTEM}")
MESSAGE(STATUS "compiler is ${CMAKE_CXX_COMPILER_ID}")

include_directories(superdog_server "third/include/libevent/include")
add_executable(superdog_server main.cpp tcp_client.cpp tcp_server_libevent.cpp tcp_swarm_libevent.cpp) 
target_link_directories(superdog_server PUBLIC "./third/lib")
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
target_link_libraries(superdog_server event.lib  event_extra.lib event_core.lib ws2_32)
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
target_link_libraries(superdog_server event.a  event_extra.a libevent_pthreads.a  event_core.a pthread)
endif()


project(swarm_client)
add_executable(swarm_client swarm.cpp tcp_client.cpp tcp_server_libevent.cpp tcp_swarm_libevent.cpp) 
target_link_directories(swarm_client PUBLIC "./third/lib")
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
target_link_libraries(swarm_client event.lib  event_extra.lib event_core.lib ws2_32)
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
target_link_libraries(swarm_client event.a  event_extra.a event_core.a libevent_pthreads.a pthread)
endif()

