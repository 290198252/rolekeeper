/*
 * @Author: your name
 * @Date: 2021-07-04 16:39:42
 * @LastEditTime: 2021-07-06 20:21:10
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \server\tcp_swarm_libevent.h
 */

#ifndef GENERAL_TCPCLIENT_H
#define GENERAL_TCPCLIENT_H

#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0500
#endif
#ifdef linux
#include<sys/types.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#define EVENT__HAVE_PTHREADS
#endif

extern  "C"{
    #include "event2/bufferevent.h"
    #include "event2/buffer.h"
    #include "event2/listener.h"
    #include "event2/util.h"
    #include "event2/event.h"
    #include "event2/thread.h"
};

#include <iostream>
#include<map>
#include <mutex>
#include <thread>
using  namespace std;

class TcpSwarmClientLibevent {
public:
    typedef enum {
		UNCONNECTED,  // 未连接
		CONNECTED, //已经连接
		FAIL,       // 连接失败
    }Status;
    friend int thread_dispatch(TcpSwarmClientLibevent *p);
    friend void conn_eventcb(struct bufferevent *bev, short events, void *user_data);    
    TcpSwarmClientLibevent(int count);
    int ConnectToServer(string server, int port);
    ~TcpSwarmClientLibevent(){
        event_base_free(m_base);
    };
    int ConnectServer();
    bool Connected();
    int Close();
    Status mStatus;
private:
    bool mReConnect = false;
    int sendData(void*,size_t);
    struct event_base *m_base;
    struct bufferevent* m_bev;
    struct sockaddr_in m_addr;
    std::thread *m_thread;
    map<evutil_socket_t,struct bufferevent*> m_clients;
    mutex m_lock;
    int addConection(evutil_socket_t fd,struct bufferevent*);
    uint32_t m_count;
    int removeConection(evutil_socket_t fd);
};
#endif //GENERAL_TCPCLIENT_H
